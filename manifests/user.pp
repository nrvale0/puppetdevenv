define puppetdevenv::user(
  $home_dir = "/home/${name}",
) {

  $bash_aliases = "${home_dir}/.bash_aliases"

  File { owner => $name, group => $name, mode => '0700', }

  file { "${bash_aliases}.d": ensure => directory,  }

  exec { 'copy old .bash_aliases to .bash_aliases.d':
    path => ['/bin','/sbin','/usr/bin','/usr/sbin'],
    command => "cp ${bash_aliases} ${home_dir}/.bash_aliases.d/00_local_aliases",
    unless => "test -f ${home_dir}/.bash_aliases.d/00_local_aliases",
    onlyif => "test -f ${home_dir}/.bash_aliases",
    require => File["${bash_aliases}.d"],
  }

  file { $bash_aliases:
    ensure => file,
    backup => '.backup',
    content => template('puppetdevenv/warning.erb','puppetdevenv/user/bash_aliases.erb'),
    require => Exec['copy old .bash_aliases to .bash_aliases.d'],
  }
  
  file { "${bash_aliases}.d/puppetdevenv":
    ensure => file,
    content => template('puppetdevenv/warning.erb', 'puppetdevenv/user/puppet_bash_aliases.erb')
  }
}

